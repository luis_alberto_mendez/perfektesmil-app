import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  public selectedIndex = 0;
  public appPages = [
    {
      title: 'Inicio',
      url: '/',
      icon: 'home'
    },
    {
      title: 'En verificación',
      url: '/verificacion',
      icon: 'eye'
    },
    {
      title: 'En diseño',
      url: '/diseno',
      icon: 'albums'
    },
    {
      title: 'Diseño finalizado',
      url: '/diseno-finalizado',
      icon: 'apps'
    },
    {
      title: 'Rediseño finalizado',
      url: '/rediseno-finalizado',
      icon: 'aperture'
    },
    {
      title: 'Rediseños solicitados',
      url: '/redisenos-solicitados',
      icon: 'copy'
    },
    {
      title: 'En producción',
      url: '/produccion',
      icon: 'construct'
    },
    {
      title: 'Facturado',
      url: '/facturado',
      icon: 'cash'
    },
    {
      title: 'Entregado en clínica',
      url: '/entregado',
      icon: 'file-tray'
    },
    {
      title: 'Mi perfil',
      url: '/perfil',
      icon: 'body'
    }
  ];
  public labels = ['Family', 'Friends', 'Notes', 'Work', 'Travel', 'Reminders'];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  ngOnInit() {
    const path = window.location.pathname.split('folder/')[1];
    if (path !== undefined) {
      this.selectedIndex = this.appPages.findIndex(page => page.title.toLowerCase() === path.toLowerCase());
    }
  }
}
