import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-detalle-caso',
  templateUrl: './detalle-caso.page.html',
  styleUrls: ['./detalle-caso.page.scss'],
})
export class DetalleCasoPage implements OnInit {
  caso = {
    imagen: '../../../assets/imgs/dientes.jpg'
  };

  constructor() { }

  ngOnInit() {
  }

}
