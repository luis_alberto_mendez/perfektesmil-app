import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetalleCasoPageRoutingModule } from './detalle-caso-routing.module';

import { DetalleCasoPage } from './detalle-caso.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetalleCasoPageRoutingModule
  ],
  declarations: [DetalleCasoPage]
})
export class DetalleCasoPageModule {}
