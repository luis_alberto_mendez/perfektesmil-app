import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetalleCasoPage } from './detalle-caso.page';

const routes: Routes = [
  {
    path: '',
    component: DetalleCasoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetalleCasoPageRoutingModule {}
