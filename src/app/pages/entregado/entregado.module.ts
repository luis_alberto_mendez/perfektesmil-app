import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EntregadoPageRoutingModule } from './entregado-routing.module';

import { EntregadoPage } from './entregado.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EntregadoPageRoutingModule
  ],
  declarations: [EntregadoPage]
})
export class EntregadoPageModule {}
