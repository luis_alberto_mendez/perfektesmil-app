import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EntregadoPage } from './entregado.page';

const routes: Routes = [
  {
    path: '',
    component: EntregadoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EntregadoPageRoutingModule {}
