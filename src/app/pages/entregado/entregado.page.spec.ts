import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EntregadoPage } from './entregado.page';

describe('EntregadoPage', () => {
  let component: EntregadoPage;
  let fixture: ComponentFixture<EntregadoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EntregadoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EntregadoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
