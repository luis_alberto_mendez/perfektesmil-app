import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FacturadoPage } from './facturado.page';

describe('FacturadoPage', () => {
  let component: FacturadoPage;
  let fixture: ComponentFixture<FacturadoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacturadoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FacturadoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
