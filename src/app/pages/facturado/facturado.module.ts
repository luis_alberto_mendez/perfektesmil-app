import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FacturadoPageRoutingModule } from './facturado-routing.module';

import { FacturadoPage } from './facturado.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FacturadoPageRoutingModule
  ],
  declarations: [FacturadoPage]
})
export class FacturadoPageModule {}
