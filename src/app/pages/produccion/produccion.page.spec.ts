import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ProduccionPage } from './produccion.page';

describe('ProduccionPage', () => {
  let component: ProduccionPage;
  let fixture: ComponentFixture<ProduccionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProduccionPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProduccionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
