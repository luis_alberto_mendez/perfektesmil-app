import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RedisenoFinalizadoPageRoutingModule } from './rediseno-finalizado-routing.module';

import { RedisenoFinalizadoPage } from './rediseno-finalizado.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RedisenoFinalizadoPageRoutingModule
  ],
  declarations: [RedisenoFinalizadoPage]
})
export class RedisenoFinalizadoPageModule {}
