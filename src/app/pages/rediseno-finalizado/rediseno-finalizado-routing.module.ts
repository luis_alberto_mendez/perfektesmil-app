import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RedisenoFinalizadoPage } from './rediseno-finalizado.page';

const routes: Routes = [
  {
    path: '',
    component: RedisenoFinalizadoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RedisenoFinalizadoPageRoutingModule {}
