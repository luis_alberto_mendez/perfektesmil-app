import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RedisenoFinalizadoPage } from './rediseno-finalizado.page';

describe('RedisenoFinalizadoPage', () => {
  let component: RedisenoFinalizadoPage;
  let fixture: ComponentFixture<RedisenoFinalizadoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RedisenoFinalizadoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RedisenoFinalizadoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
