import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DisenoFinalizadoPageRoutingModule } from './diseno-finalizado-routing.module';

import { DisenoFinalizadoPage } from './diseno-finalizado.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DisenoFinalizadoPageRoutingModule
  ],
  declarations: [DisenoFinalizadoPage]
})
export class DisenoFinalizadoPageModule {}
