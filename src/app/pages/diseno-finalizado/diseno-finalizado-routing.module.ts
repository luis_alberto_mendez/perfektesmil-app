import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DisenoFinalizadoPage } from './diseno-finalizado.page';

const routes: Routes = [
  {
    path: '',
    component: DisenoFinalizadoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DisenoFinalizadoPageRoutingModule {}
