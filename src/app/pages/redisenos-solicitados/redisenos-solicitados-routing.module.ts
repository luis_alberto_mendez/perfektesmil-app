import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RedisenosSolicitadosPage } from './redisenos-solicitados.page';

const routes: Routes = [
  {
    path: '',
    component: RedisenosSolicitadosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RedisenosSolicitadosPageRoutingModule {}
