import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RedisenosSolicitadosPageRoutingModule } from './redisenos-solicitados-routing.module';

import { RedisenosSolicitadosPage } from './redisenos-solicitados.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RedisenosSolicitadosPageRoutingModule
  ],
  declarations: [RedisenosSolicitadosPage]
})
export class RedisenosSolicitadosPageModule {}
