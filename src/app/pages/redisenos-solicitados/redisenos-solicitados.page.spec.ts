import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RedisenosSolicitadosPage } from './redisenos-solicitados.page';

describe('RedisenosSolicitadosPage', () => {
  let component: RedisenosSolicitadosPage;
  let fixture: ComponentFixture<RedisenosSolicitadosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RedisenosSolicitadosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RedisenosSolicitadosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
