import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DisenoPage } from './diseno.page';

describe('DisenoPage', () => {
  let component: DisenoPage;
  let fixture: ComponentFixture<DisenoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisenoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DisenoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
