import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'inicio',
    pathMatch: 'full'
  },
  {
    path: 'folder/:id',
    loadChildren: () => import('./folder/folder.module').then( m => m.FolderPageModule)
  },
  {
    path: 'verificacion',
    loadChildren: () => import('./pages/verificacion/verificacion.module').then( m => m.VerificacionPageModule)
  },
  {
    path: 'diseno',
    loadChildren: () => import('./pages/diseno/diseno.module').then( m => m.DisenoPageModule)
  },
  {
    path: 'diseno-finalizado',
    loadChildren: () => import('./pages/diseno-finalizado/diseno-finalizado.module').then( m => m.DisenoFinalizadoPageModule)
  },
  {
    path: 'rediseno-finalizado',
    loadChildren: () => import('./pages/rediseno-finalizado/rediseno-finalizado.module').then( m => m.RedisenoFinalizadoPageModule)
  },
  {
    path: 'redisenos-solicitados',
    loadChildren: () => import('./pages/redisenos-solicitados/redisenos-solicitados.module').then( m => m.RedisenosSolicitadosPageModule)
  },
  {
    path: 'produccion',
    loadChildren: () => import('./pages/produccion/produccion.module').then( m => m.ProduccionPageModule)
  },
  {
    path: 'facturado',
    loadChildren: () => import('./pages/facturado/facturado.module').then( m => m.FacturadoPageModule)
  },
  {
    path: 'entregado',
    loadChildren: () => import('./pages/entregado/entregado.module').then( m => m.EntregadoPageModule)
  },
  {
    path: 'inicio',
    loadChildren: () => import('./pages/inicio/inicio.module').then( m => m.InicioPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'detalle-caso',
    loadChildren: () => import('./pages/detalle-caso/detalle-caso.module').then( m => m.DetalleCasoPageModule)
  },
  {
    path: 'perfil',
    loadChildren: () => import('./pages/perfil/perfil.module').then( m => m.PerfilPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
